var myData =
{
  init: function () {
    this.loadProvince();
  },
  loadProvince: function () {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://vapi.vnappmob.com/api/province/', true);
    xhr.onload = function () {
      var provinces = JSON.parse(xhr.responseText);
      provinces.results.forEach(setOption);
      function setOption(item) {
        var op = document.createElement('option');
        op.innerText = item.province_name;
        op.setAttribute('value', item.province_id);
        document.getElementById('province').appendChild(op);
      }
    }
    xhr.send();
  }
}

function loadDistrict(province_id) {
  if (document.getElementById('province')?.value !== 0) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://vapi.vnappmob.com/api/province/district/' + document.getElementById('province')?.value, true);
    document.getElementById('district').innerHTML = '';
    xhr.onload = function () {

      var districts = JSON.parse(xhr.responseText);
      districts.results.forEach(setOption);
      function setOption(item) {
        var op = document.createElement('option');
        op.innerText = item.district_name;
        op.setAttribute('value', item.district_id);
        document.getElementById('district').appendChild(op);
      }
      
    }
    xhr.send();
  }
}

function loadWard(district_id) {
  if (document.getElementById('district')?.value !== 0)
    console.log("🚀 ~ file: province.js ~ line 27 ~ document.getElementById('district')?.value", document.getElementById('district')?.value)
  {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://vapi.vnappmob.com/api/province/ward/' + document.getElementById('district')?.value, true);
    document.getElementById('ward').innerHTML = '';
    xhr.onload = function () {
      var wards = JSON.parse(xhr.responseText);
      wards.results.forEach(setOption);
      function setOption(item) {
        var op = document.createElement('option');
        op.innerText = item.ward_name;
        op.setAttribute('value', item.ward_id);
        document.getElementById('ward').appendChild(op);
      }
    }
    xhr.send();
  }
}
myData.init();
